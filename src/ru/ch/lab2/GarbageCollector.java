package ru.ch.lab2;
/**
 * Java Core2
 * Загрузчики классов, верификация байт-кода, сборщики мусора, finalize
 *
 * Лабораторная работа №2
 */
public class GarbageCollector {
    public static void main(String[] args) {
        Object a = new Integer(100);
        Object b = new Long(100);
        Object c = new String("100");
        a = null;
        a = c;
        c = b;
        b = a; // Line7
        //один объект Integer
    }
}
