package ru.ch.lab1;

import java.io.File;

public class Main {
    public static void main(String[] args) {
        String modulePath = "D:\\development\\IdeaProjectsII\\SomeFileProject\\src\\" +
                "main\\java\\ru\\chelnokov\\study\\lab3\\";

        File dir = new File(modulePath);
        String[] modules = dir.list();

        MyLoader loader = new MyLoader(modulePath, ClassLoader.getSystemClassLoader());

        assert modules != null;
        for (String module : modules) {
            try {
                String moduleName = module.split("\\.class")[0];
                Class<?> clazz = loader.loadClass(moduleName);
                System.out.println(clazz.getName());

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
